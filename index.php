<!DOCTYPE html>
<head>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="Stylesheet" type="text/css" href="./css/style.css" />
<link href="https://fonts.googleapis.com/css?family=Roboto:500&amp;subset=latin-ext" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Anton|Catamaran:200,300,400,500,600,700|Gloria+Hallelujah|Permanent+Marker&amp;subset=latin-ext" rel="stylesheet"><script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  
</head>
<body>
    
    
    <div class="container-fluid">
       
    <header>
        <section id="welcome">
            <nav class="navbar">
                    <ul>
                        <li><a class="to" href="#welcome"><img id="hom" src="jpgs/home.png"/> </a></li>
                            <li><a class="to" href="#aboutMe">About me</a></li>
                            <li><a class="to" href="#skills" onclick="blockAnimation(this)">Skills</a></li>
                            <li><a class="to" href="#contact">Contact</a></li>
                    </ul>
                
            </nav>	
            
            <div class="introduction">
                    <h1>Paulina Barczak</h1>
                    <p class="veb">Web Developer - to be:)</p>
            </div>
        </section>
    </header>
    <section id="aboutMe">
            <h2>Few words about me</h2>
            <div class="about">
                <div><img src="./jpgs/foto.png" alt="ja" class="me"/></div>
                    <div class="social">
                            <a href="https://www.freecodecamp.org/paulinabarczak" target="_blank"><img src="https://s25.postimg.org/avcfvvxnj/fcc.png" alt="fcc" class="socialIcon" style="position: relative; left: -65px; top: 15px"/></a>
                            <a href="https://bitbucket.org/PaulinaBarczak/"><img src="./jpgs/bb.png" alt="bitbucket" class="socialIcon" style="position: relative; left: -30px; bottom: -10px;"/></a>
                            <a href="https://paulinabarczak.deviantart.com/"><img src="./jpgs/dev.png" alt="deviantArt" class="socialIcon" style="position: relative; left: -30px; bottom: -10px;"/></a>
                            <a href="https://www.linkedin.com/in/paulina-barczak/"><img src="https://s25.postimg.org/pwjbqw5r3/linkedin.png" alt="in" class="socialIcon" style="position: relative; left: -70px; bottom: -5px"/></a>
                    </div>
               
                <p> My motto is "Try to be the best version of yourself", both in personal life and work. Since I decided to rebrand myself, I know I am on the right course to achieve it. <br/><br/>Also, I believe my psychological knowledge and experience can be a great addition to a programmer's skills. <br/>I am a very social person, who loves traveling and music. <br/>Feel free to check out some of the provided links.</p> 

            </div>
    </section>
    <section id="skills">
            <h2>Skills</h2>
            <div class="programme">
                <p><b>I participated in a course organised by the PFIG foundation and Gdański Urząd Pracy in cooperation with Software Development Academy. During this seven-weeks challenge, the following subjects were covered:    </b></p>
                <ul id="list">
                    <li>Basics of HTML and CSS</li>
                    <li>Introduction to alghoritms using Java Script</li>
                    <li>Differences between frone-end and back-end programming: using PHP and Javascript</li>
                    <li>Functional programming and object oriented programming </li>
                    <li>Introduction to WWW servers, DOM and HTTP</li>
                    <li>Basics of GIT - Bitbucket, GitBash and Sourcetree</li>
                    <li>Responsive Web Design, CSS frameworks </li>
                    <li>Basic programming in Java Script, DOM</li>
                    <li>SASS</li>
                    <li>LAMP</li>
                    <li>Introduction to jQuery</li>
                    <li>Alghoritms and data structures</li>
                    <li>Relational and non-relational databases</li>
                    <li>Django framework</li>
                </ul>
            </div>
          
    </section>
    
    <section id="contact">
       
        <div class="contForm">
                     
            <h2>CONTACT</h2> 
            <p>How do you like this page? Feel free to leave me a message! </p>
                        
            <form class="cmxform" id="commentForm" method="get" action="mailto:pparuszynska@gmail.com">
    
                <input id="cname" name="name" minlength="3" type="text" placeholder="Name" required><br/>
                <input id="cemail" type="email" name="email" placeholder="Email" required><br/>
                <textarea id="ccomment" name="comment" placeholder="Your message" required></textarea><br/>
                <input type="submit" class="submit" value="Submit">
            </form>
                            
<script>
$("#commentForm").validate();
</script>

        </div>
        
    </section>
    
    <footer>
        <p id="author"> Paulina Barczak 2017</p>
        <p class="free">Icons from<a href="http://www.freepik.com"> Freepik</a>
    </footer>
    </div>
    
<script src="./js/main.js"></script>  


</body>